**VMnet 8: NAT**

- Når der sniffes på ydersiden af VMware på host-siden, kan vi ikke se Kali's IP adresse. Fordi routeren NAT 'er, gemmes Kalis IP for ydersiden. Det vil se ud som om al aktivitet kommer fra host


**VMnet 1: Host only**
- Et netværk mellem Host og VM igennem en virtuel ethernet adapter som kun er synlig for host. 

**VMnet 0: Bridging**
- VM er forbundet til Hostens subnet via hostens netværks adapter. giver VM adgang til netværket.
